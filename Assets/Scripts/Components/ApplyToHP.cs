﻿using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class ApplyToHP : MonoBehaviour, IAbilityTarget, IConvertGameObjectToEntity
{
    public List<GameObject> Targets { get; set; }

    [SerializeField] private int hp = 50;

    private EntityManager _dstManager;

    public void Execute(Entity entity)
    {
        foreach (var target in Targets)
        {
            var health = target.GetComponent<CharacterHealthConvertToEntity>();
            if (health != null)
            {
                health.SetCharacterHealthEntity(hp);
                hp = 0;
                Destroy(gameObject);
                _dstManager.DestroyEntity(entity);
            }
        }
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
    }
}

